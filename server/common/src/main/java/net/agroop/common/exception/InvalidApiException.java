package net.agroop.common.exception;

public class InvalidApiException extends Exception{
    public InvalidApiException() {
    }

    public InvalidApiException(String message) {
        super(message);
    }

    public InvalidApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidApiException(Throwable cause) {
        super(cause);
    }

    public InvalidApiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
