package net.agroop.service.device;

import net.agroop.common.exception.DeviceNotFoundException;
import net.agroop.common.exception.InvalidApiException;
import net.agroop.common.exception.UserNotFoundException;
import net.agroop.domain.api.ApiKey;
import net.agroop.domain.device.Device;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface DeviceService {
    Device createDevice(String name, Date date, Long ownerId, UUID apiKey) throws UserNotFoundException, InvalidApiException;
    Device updateDevice(Long id, String name, Date date, Long ownerId, UUID apiKey) throws UserNotFoundException, InvalidApiException, DeviceNotFoundException;
    Device getDeviceByID(Long id, UUID apiKey) throws UserNotFoundException, InvalidApiException, DeviceNotFoundException;
    List<Device> getDevicesByOwner(Long ownerId, UUID apiKey) throws UserNotFoundException, InvalidApiException;
    void delete(Long id, Long ownerId, UUID apiKey);
}
