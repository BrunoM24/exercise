package net.agroop.service.api;

import net.agroop.domain.api.ApiKey;

/**
 * ApiKeyService.java
 * Created by Rúben Madeira on 05/01/2018 - 11:10.
 * Copyright 2018 © eAgroop,Lda
 */
public interface ApiKeyService {
    ApiKey save(ApiKey apiKey);
}
