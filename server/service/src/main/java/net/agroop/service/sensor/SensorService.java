package net.agroop.service.sensor;

import net.agroop.common.exception.DeviceNotFoundException;
import net.agroop.common.exception.InvalidApiException;
import net.agroop.common.exception.UserNotFoundException;
import net.agroop.domain.sensor.SensorData;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface SensorService {
    SensorData createSensorData(Long ownerId, Long deviceId, String sensorType, Double sensorValue, Date date, UUID api_key) throws UserNotFoundException, InvalidApiException, DeviceNotFoundException;
    List<SensorData> getSensorDataBetweenTimestamps(Long ownerId, Long deviceId, Long startDate, Long endDate, UUID api_key) throws UserNotFoundException, InvalidApiException, DeviceNotFoundException;
}
