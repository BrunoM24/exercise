package net.agroop.service.user;

import net.agroop.common.exception.InvalidPasswordException;
import net.agroop.common.exception.UserEmailExistException;
import net.agroop.common.exception.UserNotFoundException;
import net.agroop.domain.user.User;

/**
 * UserService.java
 * Created by Rúben Madeira on 05/01/2018 - 11:05.
 * Copyright 2018 © eAgroop,Lda
 */
public interface UserService {
    User createUser(String name, String email, String password) throws UserEmailExistException;
    User save(User user);
    User getUser(String name, String password) throws UserNotFoundException, InvalidPasswordException;
}

