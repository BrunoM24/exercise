package net.agroop.response;

import net.agroop.domain.sensor.SensorData;

import java.util.ArrayList;
import java.util.List;

public class SensorsDataBetweenTimeStampsResponse {

    private List<SensorDataResponse> list = new ArrayList<>();

    public SensorsDataBetweenTimeStampsResponse(List<SensorData> list) {
        populate(list);
    }

    private void populate(List<SensorData> list) {
        for(SensorData sensorData : list){
            this.list.add(new SensorDataResponse(sensorData));
        }
    }

    public List<SensorDataResponse> getList() {
        return list;
    }

    public void setList(List<SensorDataResponse> list) {
        this.list = list;
    }
}
