package net.agroop.response;

import net.agroop.domain.sensor.SensorData;

import java.sql.Timestamp;
import java.util.Date;

public class SensorDataResponse {

    private Long deviceId;
    private Long sensorDataId;
    private String sensorType;
    private Double sensorValue;
    private Date date;

    public SensorDataResponse(SensorData sensorData) {
        this.deviceId = sensorData.getDevice().getId();
        this.sensorDataId = sensorData.getId();
        this.sensorType = sensorData.getSensorTypeString();
        this.sensorValue = sensorData.getSensorValue();
        this.date = sensorData.getDate();
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Long getSensorDataId() {
        return sensorDataId;
    }

    public void setSensorDataId(Long sensorDataId) {
        this.sensorDataId = sensorDataId;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public Double getSensorValue() {
        return sensorValue;
    }

    public void setSensorValue(Double sensorValue) {
        this.sensorValue = sensorValue;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
