package net.agroop.response;

import java.util.Date;

public class DeviceResponse {

    private Long deviceId;
    private String deviceName;
    private Date dateOfCreation;

    public DeviceResponse(Long deviceId, String deviceName, Date dateOfCreation) {
        this.deviceId = deviceId;
        this.deviceName = deviceName;
        this.dateOfCreation = dateOfCreation;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }
}
