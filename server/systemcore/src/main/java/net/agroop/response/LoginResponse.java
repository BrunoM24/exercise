package net.agroop.response;

import net.agroop.domain.user.User;

import java.util.UUID;

public class LoginResponse {

    private long userId;
    private UUID apiKey;

    public LoginResponse(long userId, UUID apiKey) {
        this.userId = userId;
        this.apiKey = apiKey;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public UUID getApiKey() {
        return apiKey;
    }

    public void setApiKey(UUID apiKey) {
        this.apiKey = apiKey;
    }
}
