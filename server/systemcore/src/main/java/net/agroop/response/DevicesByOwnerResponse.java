package net.agroop.response;

import net.agroop.domain.device.Device;

import java.util.ArrayList;
import java.util.List;

public class DevicesByOwnerResponse {

    private List<DeviceResponse> deviceResponses = new ArrayList<>();

    public DevicesByOwnerResponse(List<Device> devices) {

        for(Device device : devices){
            this.deviceResponses.add(new DeviceResponse(device.getId(), device.getNameOfDevice(), device.getDateOfCreation()));
        }
    }

    public List<DeviceResponse> getDeviceResponses() {
        return deviceResponses;
    }

    public void setDeviceResponses(List<DeviceResponse> deviceResponses) {
        this.deviceResponses = deviceResponses;
    }
}
