package net.agroop.response;

import java.util.UUID;

/**
 * RegisterResponse.java
 * Created by José Garção on 08/01/2018 - 10:53.
 * Copyright 2018 © eAgroop,Lda
 */
public class RegisterResponse {

    private UUID apiKey;

    public RegisterResponse(UUID apiKey) {
        this.apiKey = apiKey;
    }

    public UUID getApiKey() {
        return apiKey;
    }

    public void setApiKey(UUID apiKey) {
        this.apiKey = apiKey;
    }
}
