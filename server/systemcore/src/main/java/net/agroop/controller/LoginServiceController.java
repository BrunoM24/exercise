package net.agroop.controller;

import net.agroop.common.exception.InvalidPasswordException;
import net.agroop.common.exception.UserNotFoundException;
import net.agroop.controller.endpoint.Endpoint;
import net.agroop.domain.user.User;
import net.agroop.request.LoginRequest;
import net.agroop.response.LoginResponse;
import net.agroop.response.RegisterResponse;
import net.agroop.service.user.UserService;
import net.agroop.util.ResponseEntityHelper;
import net.agroop.util.ResponseStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;

@RestController
@RequestMapping(Endpoint.LOGIN)
public class LoginServiceController {

    private static final Logger logger = LoggerFactory.getLogger(LoginServiceController.class);

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public Callable<ResponseEntity> login(@RequestBody LoginRequest loginRequest){
        return () -> {
            try{
                User user = this.userService.getUser(loginRequest.getName(), loginRequest.getPassword());
                LoginResponse response = new LoginResponse(user.getId(), user.getApiKey().getId());
                return ResponseEntityHelper.ok(response);
            }catch (UserNotFoundException | InvalidPasswordException e){
                return ResponseEntityHelper.badRequest(ResponseStatus.withReason(e.getMessage()));
            }catch (Exception e){
                logger.error("500 SERVER ERROR", e);
                return ResponseEntityHelper.serverError();
            }
        };
    }
}
