package net.agroop.controller;

import net.agroop.common.exception.DeviceNotFoundException;
import net.agroop.common.exception.InvalidApiException;
import net.agroop.common.exception.UserNotFoundException;
import net.agroop.controller.endpoint.Endpoint;
import net.agroop.domain.device.Device;
import net.agroop.request.DeviceRequest;
import net.agroop.request.DeviceUpdateRequest;
import net.agroop.response.DeviceResponse;
import net.agroop.response.DevicesByOwnerResponse;
import net.agroop.service.device.DeviceService;
import net.agroop.util.ResponseEntityHelper;
import net.agroop.util.ResponseStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;

@RestController
@RequestMapping(Endpoint.DEVICE)
public class DeviceServiceController {

    private static final Logger logger = LoggerFactory.getLogger(DeviceServiceController.class);

    private DeviceService deviceService;

    @Autowired
    public void setDeviceService(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @RequestMapping(value = "/{api_key}", method = RequestMethod.GET)
    public Callable<ResponseEntity> get(@PathVariable(value = "api_key")UUID api_key, @RequestParam("id") Long deviceId){
       return () -> {
           try {
               Device device = this.deviceService.getDeviceByID(deviceId, api_key);
               DeviceResponse response = new DeviceResponse(device.getId(), device.getNameOfDevice(), device.getDateOfCreation());
               return ResponseEntityHelper.ok(response);
           }catch (DeviceNotFoundException | InvalidApiException e){
               return ResponseEntityHelper.badRequest(ResponseStatus.withReason(e.getMessage()));
           }catch (Exception e){
               logger.error("500 SERVER ERROR", e);
               return ResponseEntityHelper.serverError();
           }
       };
    }

    @RequestMapping(value = "/byOwner/{api_key}", method = RequestMethod.GET)
    public Callable<ResponseEntity> list(@PathVariable(value = "api_key") UUID api_key, @RequestParam("userId") Long userId){
        return () -> {
            try {
                List<Device> devices = this.deviceService.getDevicesByOwner(userId, api_key);
                DevicesByOwnerResponse response = new DevicesByOwnerResponse(devices);
                return ResponseEntityHelper.ok(response);
            }catch (UserNotFoundException | InvalidApiException e){
                return ResponseEntityHelper.badRequest(ResponseStatus.withReason(e.getMessage()));
            }catch (Exception e){
                logger.error("500 SERVER ERROR", e);
                return ResponseEntityHelper.serverError();
            }
        };
    }

    @RequestMapping(method = RequestMethod.POST)
    public Callable<ResponseEntity> create(@RequestBody DeviceRequest request){
        return () -> {
            try {
                Device device = this.deviceService.createDevice(request.getDeviceName(), request.getCreationDate(), request.getOwnerId(), request.getApiKey());
                DeviceResponse response = new DeviceResponse(device.getId(), device.getNameOfDevice(), device.getDateOfCreation());
                return ResponseEntityHelper.ok(response);
            }catch (UserNotFoundException | InvalidApiException e){
                return ResponseEntityHelper.badRequest(ResponseStatus.withReason(e.getMessage()));
            }catch (Exception e){
                logger.error("500 SERVER ERROR", e);
                return ResponseEntityHelper.serverError();
            }
        };
    }

    @RequestMapping(method = RequestMethod.PUT)
    public Callable<ResponseEntity> update(@RequestBody DeviceUpdateRequest request){
        return () -> {
            try {
                Device device = this.deviceService.updateDevice(request.getDeviceId(), request.getDeviceName(), request.getCreationDate(), request.getOwnerId(), request.getApi_key());
                DeviceResponse response = new DeviceResponse(device.getId(), device.getNameOfDevice(), device.getDateOfCreation());
                return ResponseEntityHelper.ok(response);
            }catch (UserNotFoundException | InvalidApiException | DeviceNotFoundException e){
                return ResponseEntityHelper.badRequest(ResponseStatus.withReason(e.getMessage()));
            }catch (Exception e){
                logger.error("500 SERVER ERROR", e);
                return ResponseEntityHelper.serverError();
            }
        };
    }
}
