package net.agroop.controller;

import net.agroop.common.exception.DeviceNotFoundException;
import net.agroop.common.exception.InvalidApiException;
import net.agroop.common.exception.UserNotFoundException;
import net.agroop.controller.endpoint.Endpoint;
import net.agroop.domain.sensor.SensorData;
import net.agroop.request.SensorDataRequest;
import net.agroop.response.SensorDataResponse;
import net.agroop.response.SensorsDataBetweenTimeStampsResponse;
import net.agroop.service.sensor.SensorService;
import net.agroop.util.ResponseEntityHelper;
import net.agroop.util.ResponseStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;

@RestController
@RequestMapping(Endpoint.SENSOR)
public class SensorServiceController {

    private static final Logger logger = LoggerFactory.getLogger(SensorServiceController.class);

    private SensorService sensorService;

    @Autowired
    public void setSensorService(SensorService sensorService) {
        this.sensorService = sensorService;
    }

    @RequestMapping(value = "/{api_key}", method = RequestMethod.GET)
    public Callable<ResponseEntity> getBetweenTimestamps(@PathVariable(value = "api_key") UUID api_key, @RequestParam("userId") Long userId, @RequestParam("deviceId") Long deviceId, @RequestParam("startDate") Long startDate, @RequestParam("endDate")Long endDate){
        return () -> {
            try {
                List<SensorData> list = this.sensorService.getSensorDataBetweenTimestamps(userId, deviceId, startDate, endDate, api_key);
                SensorsDataBetweenTimeStampsResponse response = new SensorsDataBetweenTimeStampsResponse(list);
                return ResponseEntityHelper.ok(response);
            }catch (UserNotFoundException | DeviceNotFoundException | InvalidApiException e){
                return ResponseEntityHelper.badRequest(ResponseStatus.withReason(e.getMessage()));
            }catch (Exception e){
                logger.error("500 SERVER ERROR", e);
                return ResponseEntityHelper.serverError();
            }
        };
    }

    @RequestMapping(value = "/{api_key}", method = RequestMethod.POST)
    public Callable<ResponseEntity> create(@PathVariable(value = "api_key")UUID api_key, @RequestBody SensorDataRequest request){
        return () -> {
            try {
                SensorData sensorData = this.sensorService.createSensorData(request.getOwnerId(), request.getDeviceId(), request.getSensorType(), request.getSensorValue(), request.getDate(), api_key);
                SensorDataResponse response = new SensorDataResponse(sensorData);
                return ResponseEntityHelper.ok(response);
            }catch (UserNotFoundException | DeviceNotFoundException | InvalidApiException e){
                return ResponseEntityHelper.badRequest(ResponseStatus.withReason(e.getMessage()));
            }catch (Exception e){
                logger.error("500 SERVER ERROR", e);
                return ResponseEntityHelper.serverError();
            }
        };
    }
}
