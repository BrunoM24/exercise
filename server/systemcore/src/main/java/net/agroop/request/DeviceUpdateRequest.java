package net.agroop.request;

import java.util.Date;
import java.util.UUID;

public class DeviceUpdateRequest {

    private Long deviceId;
    private String deviceName;
    private Date creationDate;
    private Long ownerId;
    private UUID api_key;

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public UUID getApi_key() {
        return api_key;
    }

    public void setApi_key(UUID api_key) {
        this.api_key = api_key;
    }
}
