package net.agroop.request;

import java.util.UUID;

public class DeviceByIdRequest {
    private UUID api_key;
    private Long ownerId;

    public UUID getApi_key() {
        return api_key;
    }

    public void setApi_key(UUID api_key) {
        this.api_key = api_key;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }
}
