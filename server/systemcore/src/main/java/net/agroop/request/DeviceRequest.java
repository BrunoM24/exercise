package net.agroop.request;

import java.util.Date;
import java.util.UUID;

public class DeviceRequest {

    private String deviceName;
    private Date creationDate;
    private Long ownerId;
    private UUID apiKey;

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public UUID getApiKey() {
        return this.apiKey;
    }

    public void setApiKey(UUID api_key) {
        this.apiKey = api_key;
    }
}
