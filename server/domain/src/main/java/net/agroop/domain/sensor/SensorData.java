package net.agroop.domain.sensor;

import net.agroop.domain.device.Device;

import javax.persistence.*;
import java.util.Date;

@Entity
public class SensorData {

    private enum SensorType {
        TEMP,
        HUM,
        RAINFALL,
    }

    @Id
    @GeneratedValue
    private Long id;

    @Enumerated(EnumType.STRING)
    private SensorType sensorType;

    private Double sensorValue;

    @ManyToOne
    private Device device;

    private Date date;

    public SensorData() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SensorType getSensorType() {
        return sensorType;
    }

    public void setSensorType(SensorType sensorType) {
        this.sensorType = sensorType;
    }

    public void setSensorType(String sensorType){
        this.sensorType = SensorType.valueOf(sensorType);
    }

    public String getSensorTypeString() {
        return this.sensorType.name();
    }

    public Double getSensorValue() {
        return sensorValue;
    }

    public void setSensorValue(Double sensorValue) {
        this.sensorValue = sensorValue;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
