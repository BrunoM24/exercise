package net.agroop.repository.user;

import net.agroop.domain.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * UserRepository.java
 * Created by Rúben Madeira on 05/01/2018 - 10:54.
 * Copyright 2018 © eAgroop,Lda
 */
@Repository
public interface UserRepository extends JpaRepository<User,Long>{
    User findUserByEmail(String email);
    User findUserByName(String name);
}
