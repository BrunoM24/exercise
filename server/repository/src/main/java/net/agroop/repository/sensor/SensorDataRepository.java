package net.agroop.repository.sensor;

import net.agroop.domain.device.Device;
import net.agroop.domain.sensor.SensorData;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface SensorDataRepository extends JpaRepository<SensorData, Long>{
    List<SensorData> getAllByDeviceAndDateBetweenOrderByDate(Device device, Date startDate, Date endDate);
}
