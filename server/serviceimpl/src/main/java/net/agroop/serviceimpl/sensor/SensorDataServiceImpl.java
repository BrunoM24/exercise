package net.agroop.serviceimpl.sensor;

import net.agroop.common.exception.DeviceNotFoundException;
import net.agroop.common.exception.InvalidApiException;
import net.agroop.common.exception.UserNotFoundException;
import net.agroop.domain.device.Device;
import net.agroop.domain.sensor.SensorData;
import net.agroop.domain.user.User;
import net.agroop.repository.device.DeviceRepository;
import net.agroop.repository.sensor.SensorDataRepository;
import net.agroop.repository.user.UserRepository;
import net.agroop.service.sensor.SensorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class SensorDataServiceImpl implements SensorService {

    private UserRepository userRepository;
    private DeviceRepository deviceRepository;
    private SensorDataRepository sensorDataRepository;

    @Override
    @Transactional
    public SensorData createSensorData(Long ownerId, Long deviceId, String sensorType, Double sensorValue, Date date, UUID apiKey) throws UserNotFoundException, InvalidApiException, DeviceNotFoundException {

        User user = userRepository.findOne(ownerId);

        if(user == null){
            throw new UserNotFoundException("User with id " + ownerId + " not found");
        }

        if(!user.getApiKey().getId().equals(apiKey)){
            throw new InvalidApiException("Invalid Api_Key");
        }

        Device device = this.deviceRepository.findOne(deviceId);

        if(device == null){
            throw new DeviceNotFoundException("Device with id " + deviceId + " not found");
        }

        SensorData sensorData = new SensorData();
        sensorData.setDevice(device);
        sensorData.setSensorType(sensorType);
        sensorData.setSensorValue(sensorValue);
        sensorData.setDate(date);

        sensorDataRepository.save(sensorData);

        return sensorData;
    }

    @Override
    public List<SensorData> getSensorDataBetweenTimestamps(Long ownerId, Long deviceId, Long startDate, Long endDate, UUID apiKey) throws UserNotFoundException, InvalidApiException, DeviceNotFoundException {

        User user = userRepository.findOne(ownerId);

        if(user == null){
            throw new UserNotFoundException("User with id " + ownerId + " not found");
        }

        if(!user.getApiKey().getId().equals(apiKey)){
            throw new InvalidApiException("Invalid Api_Key");
        }

        Device device = this.deviceRepository.findOne(deviceId);

        if(device == null){
            throw new DeviceNotFoundException("Not find device with the id " + deviceId);
        }

        //List<SensorData> list = this.sensorDataRepository.getAllByTimestampBetween(Timestamp.valueOf(startDate), Timestamp.valueOf(endDate));
        //List<SensorData> list = this.sensorDataRepository.getAllByDeviceAndTimestampBetween(device, Timestamp.valueOf(startDate), Timestamp.valueOf(endDate));
        List<SensorData> list = this.sensorDataRepository.getAllByDeviceAndDateBetweenOrderByDate(device, new Date(startDate), new Date(endDate));

        return list;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setDeviceRepository(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    @Autowired
    public void setSensorDataRepository(SensorDataRepository sensorDataRepository) {
        this.sensorDataRepository = sensorDataRepository;
    }
}
