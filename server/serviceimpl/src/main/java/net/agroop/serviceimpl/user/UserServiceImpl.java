package net.agroop.serviceimpl.user;

import net.agroop.common.exception.InvalidPasswordException;
import net.agroop.common.exception.UserEmailExistException;
import net.agroop.common.exception.UserNotFoundException;
import net.agroop.domain.api.ApiKey;
import net.agroop.domain.user.User;
import net.agroop.repository.user.UserRepository;
import net.agroop.service.api.ApiKeyService;
import net.agroop.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * UserServiceImpl.java
 * Created by Rúben Madeira on 05/01/2018 - 11:07.
 * Copyright 2018 © eAgroop,Lda
 */
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    UserRepository userRepository;

    @Autowired
    ApiKeyService apiKeyService;

    @Override
    @Transactional
    public User createUser(String name, String email, String password) throws UserEmailExistException {

        if(emailExist(email))
            throw new UserEmailExistException("There is an user with that email: " + email);

        User u = new User();
        ApiKey apiKey = new ApiKey();

        apiKey.setUser(u);

        u.setName(name);
        u.setEmail(email);
        u.setPassword(new BCryptPasswordEncoder().encode(password));
        u.setApiKey( apiKeyService.save(apiKey));

        return save(u);
    }

    @Override
    @Transactional
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    @Transactional
    public User getUser(String name, String password) throws UserNotFoundException, InvalidPasswordException {

        User user = userRepository.findUserByName(name);

        if(user == null){
            throw new UserNotFoundException("User not found with name: " + name);
        }

        if(!new BCryptPasswordEncoder().matches(password, user.getPassword())){
            throw new InvalidPasswordException("Invalid password");
        }

        return user;
    }

    private boolean emailExist(String email){
        return userRepository.findUserByEmail(email) != null ? true : false;
    }
}
