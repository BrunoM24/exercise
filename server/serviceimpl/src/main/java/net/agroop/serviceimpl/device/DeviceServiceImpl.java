package net.agroop.serviceimpl.device;

import net.agroop.common.exception.DeviceNotFoundException;
import net.agroop.common.exception.InvalidApiException;
import net.agroop.common.exception.UserNotFoundException;
import net.agroop.domain.api.ApiKey;
import net.agroop.domain.device.Device;
import net.agroop.domain.user.User;
import net.agroop.repository.device.DeviceRepository;
import net.agroop.repository.user.UserRepository;
import net.agroop.service.device.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class DeviceServiceImpl implements DeviceService{

    private UserRepository userRepository;
    private DeviceRepository deviceRepository;

    @Override
    @Transactional
    public Device createDevice(String name, Date date, Long ownerId, UUID apiKey) throws UserNotFoundException, InvalidApiException {

        User user = userRepository.findOne(ownerId);

        if(user == null){
            throw new UserNotFoundException("User with id " + ownerId + " not found");
        }

        if(!user.getApiKey().getId().equals(apiKey)){
            throw new InvalidApiException("Invalid Api_Key");
        }

        Device device = new Device();
        device.setNameOfDevice(name);
        device.setDateOfCreation(date);
        device.setOwner(user);

        deviceRepository.save(device);

        return device;
    }

    @Override
    @Transactional
    public Device updateDevice(Long id, String name, Date date, Long ownerId, UUID apiKey) throws UserNotFoundException, InvalidApiException, DeviceNotFoundException {

        User user = userRepository.findOne(ownerId);

        if(user == null){
            throw new UserNotFoundException("User with id " + ownerId + " not found");
        }

        if(!user.getApiKey().getId().equals(apiKey)){
            throw new InvalidApiException("Invalid Api_Key");
        }

        Device device = deviceRepository.findOne(id);

        if(device == null){
            throw new DeviceNotFoundException("Device with id " + id + " not found");
        }

        device.setNameOfDevice(name);
        device.setDateOfCreation(date);

        deviceRepository.save(device);

        return device;
    }

    @Override
    public Device getDeviceByID(Long id, UUID apiKey) throws InvalidApiException, DeviceNotFoundException {

        Device device = deviceRepository.findOne(id);

        if(device == null){
            throw new DeviceNotFoundException("Device with id " + id + " not found");
        }

        User user = device.getOwner();

        if(!user.getApiKey().getId().equals(apiKey)){
            throw new InvalidApiException("Invalid Api_Key");
        }

        return device;
    }

    @Override
    public List<Device> getDevicesByOwner(Long ownerId, UUID apiKey) throws UserNotFoundException, InvalidApiException {

        User user = userRepository.findOne(ownerId);

        if(user == null){
            throw new UserNotFoundException("User with id " + ownerId + " not found");
        }

        if(!user.getApiKey().getId().equals(apiKey)){
            throw new InvalidApiException("Invalid Api_Key");
        }

        List<Device> devices = deviceRepository.getDevicesByOwnerId(ownerId);

        return devices;
    }

    @Override
    @Transactional
    public void delete(Long id, Long ownerId, UUID apiKey) {

        User user = userRepository.getOne(ownerId);

        if(user.getApiKey().getId() != apiKey){
            //TODO throw invalid apiKey
        }

        //TODO delete all the device data

        deviceRepository.delete(id);
    }

    @Autowired
    public void setDeviceRepository(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
}
