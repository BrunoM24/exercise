import React from 'react';
import axios from 'axios';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';

import config from './config';

/**
 * Form for register one user.
 */
export default class AuthForm extends React.Component {

  state = {
    name: '',
    email: '',
    password: ''
  };

  handleInputChange = e => this.setState({[e.target.name]: e.target.value});

  handleSubmit = (e) => {
    e.preventDefault();

    axios.post(config.serverUrl + '/v1/registration', this.state)
    // Save apiKey to localStorage. Use it on the other requests you make to the server
      .then(({data}) => {
        localStorage.setItem('apiKey', data.apiKey);
        this.props.history.push("/login");
      });
  };

  render() {
    return (
      <div className="FormDiv">
        <Paper zDepth={2} className="FormPaper">
          <div className="FormHeader">
            <h1>Registration</h1>
          </div>
            <form onSubmit={this.handleSubmit}>
              <TextField required name="name" hintText="Username Field" floatingLabelText="Username" onChange={this.handleInputChange}/><br />
              <TextField required name="password" hintText="Password Field" floatingLabelText="Password" type="password" onChange={this.handleInputChange}/><br />
              <TextField required name="email" hintText="Email Field" floatingLabelText="Email" type="email" onChange={this.handleInputChange}/><br/>
              <div className="FormButtons">
                <RaisedButton label="Registration" primary={true} type="submit" style={{margin: "0 2px"}}/>
                <RaisedButton label="Login" secondary={true} href="./login" style={{margin: "0px 2px"}}/>
              </div>
            </form>
        </Paper>
      </div>
    )
  }
}
