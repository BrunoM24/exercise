import React, { Component } from 'react';
import {BrowserRouter, Route, Switch, Redirect} from 'react-router-dom'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import './App.css';
import AuthForm from "./AuthForm";
import LoginForm from "./LoginForm";
import Devices from "./Devices";
import Device from "./Device";
import Page404 from "./Page404";

class App extends Component {

  redirect = (redirectTo, needToBeLogged) => {
    if(needToBeLogged){
      if(!localStorage.getItem('apiKey') || !localStorage.getItem('userId')){
        return <Redirect to="/login"/>;
      }

      return redirectTo;
    }

    if(localStorage.getItem('apiKey') || localStorage.getItem('userId')){
      return <Redirect to="/devices"/>
    }

    return redirectTo
  }

    devicesRender = () => {return this.redirect(<Devices/>, true)}

    loginRender = () => {return this.redirect(<LoginForm/>, false)}

    registrationRender = () => {return this.redirect(<AuthForm/>, false)}

    deviceRender = () => {return this.redirect(<Device/>, true)}

    homeRender = () => {return this.redirect(<Redirect to="/devices"/>, true)}

  render() {
    return (
      <BrowserRouter>
        <MuiThemeProvider>
          <div className="Section">
            <Switch>
              <Route exact path="/registration" render={this.registrationRender} />
              <Route exact path="/login" render={this.loginRender}/>
              <Route exact path="/devices" render={this.devicesRender}/>
              <Route path="/device/:deviceId" render={this.deviceRender} />
              <Route exact path="/" render={this.homeRender}/>
              <Route path="*" component={Page404}/>
            </Switch>
          </div>
        </MuiThemeProvider>
      </BrowserRouter>
    );
  }
}

export default App;
