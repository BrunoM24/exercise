import React from 'react';
import axios from 'axios';
import {Redirect} from 'react-router-dom'
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';

import config from './config';

/**
 * Form for register one user.
 */
class LoginForm extends React.Component {

  state = {
    name: '',
    password: '',
    fireRedirect: false
  };

  handleInputChange = e => this.setState({[e.target.name]: e.target.value});

  handleSubmit = (e) => {
    e.preventDefault();

    axios.post(config.serverUrl + '/v1/login', this.state)
      .then(({data}) => {
        localStorage.setItem('apiKey', data.apiKey);
        localStorage.setItem('userId', data.userId);
        this.setState({ fireRedirect: true })
      });
  };

  render() {
    return (
      <div className="FormDiv">
        <Paper zDepth={2} className="FormPaper">
          <div className="FormHeader">
            <h1>Login</h1>
          </div>
          <form onSubmit={this.handleSubmit}>
            <TextField required name="name" hintText="Username Field" floatingLabelText="Username" onChange={this.handleInputChange}/><br />
            <TextField required name="password" hintText="Password Field" floatingLabelText="Password" type="password" onChange={this.handleInputChange}/><br />
            <div className="FormButtons">
              <RaisedButton label="Login" primary={true} type="submit" style={{margin: "0 2px"}}/>
              <RaisedButton label="Registar" secondary={true} href="./registration" style={{margin: "0 2px"}}/>
            </div>
          </form>
        </Paper>
        {this.state.fireRedirect && (
          <Redirect to={'/devices'}/>
        )}
      </div>
    )
  }
}

export default LoginForm;
