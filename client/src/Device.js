import React from 'react';
import axios from 'axios';
import DateTimePicker from 'material-ui-datetimepicker';
import DatePickerDialog from 'material-ui/DatePicker/DatePickerDialog'
import TimePickerDialog from 'material-ui/TimePicker/TimePickerDialog';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {withRouter} from "react-router-dom";

import config from './config';

/**
 * Form for register one user.
 */
class Device extends React.Component {

  state = {
    apiKey: localStorage.getItem('apiKey'),
    ownerId: localStorage.getItem('userId'),
    deviceId: this.props.match.params.deviceId,
    sensorType: "TEMP",
    sensorValue: 0.00,
    date: new Date(),
    startDate: new Date(),
    endDate: new Date(),
    data: [],
    search: false
  };

  handleInputChange = (event, index, value) => this.setState({"sensorType": value});
  handleSubmit = (e) => {
    e.preventDefault();

    axios.post(config.serverUrl + '/v1/sensor/' + localStorage.getItem("apiKey"), this.state)
      .then(({data}) => {
        console.log(data);
      });
  };

  searchBetweenTimestamps = (e) => {
    e.preventDefault();

    axios.get(config.serverUrl + "/v1/sensor/" + localStorage.getItem("apiKey"), {
      params: {
        userId: localStorage.getItem("userId"),
        deviceId: this.state.deviceId,
        startDate: this.state.startDate.getTime(),
        endDate: this.state.endDate.getTime()
        }
      })
      .then(({data}) => {
        this.setState({search : true});
        this.setState({data : data.list});
      });
  }

  render() {
    return (
      <div>
        <h1>Create a Sensor Data</h1>
        <form onSubmit={this.handleSubmit} className="CreateDeviceForm">
          <SelectField name="sensorType" className="CreateDeviceFormFields" floatingLabelText="Sensor Type" onChange={(event, index, value) => this.setState({"sensorType": value})} value={this.state.sensorType}>
            <MenuItem value="TEMP" primaryText="TEMP"/>
            <MenuItem value="HUM" primaryText="HUM"/>
            <MenuItem value="RAINFALL" primaryText="RAINFALL"/>
          </SelectField>
          {/* TODO: replace to accept only numbers */}
          <TextField
            required
            name="sensorValue"
            type="text"
            className="CreateDeviceFormFields"
            hintText="Sensor Value Field"
            floatingLabelText="Sensor Value"
            onChange={e => this.setState({[e.target.name]: e.target.value})}
            min="0.0"
            />
          <DateTimePicker
            className="CreateDeviceFormFields"
            value={this.state.date}
            format='DD-MM-YYYY HH:mm'
            DatePicker={DatePickerDialog}
            TimePicker={TimePickerDialog}
            onChange={(date) => this.setState({"date": date})}
            />
          <RaisedButton label="Create" primary={true} type="submit" className="CreateDeviceFormButton" style={{display: "table"}}/>
        </form>
        <h2>Search Data between Timestamps</h2>
        <form className="CreateDeviceForm" onSubmit={this.searchBetweenTimestamps}>
          <DateTimePicker
            id="startDate"
            className="CreateDeviceFormFields"
            value={this.state.startDate}
            format='DD-MM-YYYY HH:mm'
            DatePicker={DatePickerDialog}
            TimePicker={TimePickerDialog}
            onChange={(startDate) => this.setState({"startDate": startDate})}
            />
          <DateTimePicker
            name="endDate"
            className="CreateDeviceFormFields"
            value={this.state.endDate}
            format='DD-MM-YYYY HH:mm'
            DatePicker={DatePickerDialog}
            TimePicker={TimePickerDialog}
            onChange={(endDate) => this.setState({"endDate": endDate})}
            />
          <RaisedButton label="Search" primary={true} type="submit" className="CreateDeviceFormButton" style={{display: "table"}}/>
        </form>
        {this.state.search &&
            <SensorData data={this.state.data}/>
        }
      </div>
    )
  }
}

//<li key={data.sensorDataId}>Device ID: {data.deviceId} - SensorData ID: {data.sensorDataId} - Sensor Type: {data.sensorType} - Value: {data.sensorValue} - <CreateDate date={data.date}/></li>

function SensorData(props){
  const listItems = props.data.map((data) =>
    <TableRow key={data.sensorDataId}>
      <TableRowColumn>{data.sensorType}</TableRowColumn>
      <TableRowColumn>{data.sensorValue}</TableRowColumn>
      <TableRowColumn><CreateDate date={data.date} /></TableRowColumn>
    </TableRow>
  );

  return(
    <div>
      <h1>Sensor Data</h1>
      <Table onRowSelection={props.onRowSelection}>
        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
          <TableRow>
            <TableHeaderColumn>Sensor Type</TableHeaderColumn>
            <TableHeaderColumn>Value</TableHeaderColumn>
            <TableHeaderColumn>Date</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {listItems}
        </TableBody>
      </Table>
    </div>
  );
}

function CreateDate(props){
  var date = new Date(props.date)
  var convert = (value) => {
    if((value + "").length === 1){
      return "0" + value;
    }

    return value;
  }
  return convert(date.getDate()) + "-" + convert(date.getMonth() + 1) + "-" + date.getFullYear() + " " + convert(date.getHours())  + ":" + convert(date.getMinutes());
}

export default withRouter(Device);
