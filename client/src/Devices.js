import React from 'react';
import axios from 'axios';
import {withRouter} from 'react-router-dom'
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import DatePickerDialog from 'material-ui/DatePicker/DatePickerDialog';
import TimePickerDialog from 'material-ui/TimePicker/TimePickerDialog';
import DateTimePicker from 'material-ui-datetimepicker';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';

import config from './config';

/**
 * Form for register one user.
 */
class Devices extends React.Component {

  state = {
    deviceName: '',
    creationDate: new Date(),
    apiKey: localStorage.getItem('apiKey'),
    ownerId: localStorage.getItem('userId'),
    devices: [],
    error: false
  };

  handleInputChange = e => {
    this.setState({[e.target.name]: e.target.value});

    if(e.target.value === ""){
      this.setState({"error": true});
    }else {
      this.setState({"error": false});
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();

    axios.post(config.serverUrl + '/v1/device', this.state)
      .then(({data}) => {
        this.setState(state=>({
          devices: [...state.devices, data]
        }));
        console.log(data);
      });
  };

  componentWillMount(){
    axios.get(config.serverUrl + "/v1/device/byOwner/" + localStorage.getItem("apiKey") + "?userId=" + localStorage.getItem("userId"))
      .then(({data}) => {
        this.setState({devices: data.deviceResponses});
      });
  }

  render() {
    return (
      <div>
        <h1>Insert a Device</h1>
        <form onSubmit={this.handleSubmit} className="CreateDeviceForm">
          <TextField required className="CreateDeviceFormFields" name="deviceName" type="text" hintText="Device Name Field" floatingLabelText="Device Name" onChange={this.handleInputChange}/>
          {this.state.error && <label>The Device Name is required</label>}
          <DateTimePicker
            className="CreateDeviceFormFields"
            value={this.state.creationDate}
            format='DD-MM-YYYY HH:mm'
            DatePicker={DatePickerDialog}
            TimePicker={TimePickerDialog}
            onChange={(dateTime) => this.setState({"creationDate": dateTime })}
            />
          <RaisedButton label="Add Device" primary={true} type="submit" className="CreateDeviceFormButton" style={{display: "table"}}/>
        </form>

        <h1>Devices:</h1>
        <DevicesTable onRowSelection={index=>this.props.history.push(`/device/${this.state.devices[index].deviceId}`)} devices={this.state.devices}/>
      </div>
    )
  }
}

//<li key={device.deviceId}>Device Name: {device.deviceName} - Date: <CreateDate date={device.dateOfCreation} /> - <Link to={`/device/${device.deviceId}`}>go</Link></li>

function DevicesTable(props){
  const listItems = props.devices.map((device) =>
    <TableRow key={device.deviceId} rowNumber={device.deviceId}>
      <TableRowColumn>{device.deviceName}</TableRowColumn>
      <TableRowColumn><CreateDate date={device.dateOfCreation} /></TableRowColumn>
    </TableRow>
  );

  return (
    <Table onRowSelection={props.onRowSelection}>
      <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
        <TableRow>
          <TableHeaderColumn>Name</TableHeaderColumn>
          <TableHeaderColumn>Date</TableHeaderColumn>
        </TableRow>
      </TableHeader>
      <TableBody displayRowCheckbox={false}>
        {listItems}
      </TableBody>
    </Table>
  );
}

function CreateDate(props){
  var date = new Date(props.date)

  var convert = (value) => {
    if((value + "").length === 1){
      return "0" + value;
    }

    return value;
  }
  return convert(date.getDate()) + "-" + convert(date.getMonth()) + "-" + date.getFullYear() + " " + convert(date.getHours())  + ":" + convert(date.getMinutes());
}

export default withRouter(Devices);
